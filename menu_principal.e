note
	description: "La classe {MENU_PRINCIPAL} s'occupe du menu principal du jeu."
	author: "C�dryk Coderre et Pier-Olivier Chagnon"
	date: "2018-05-14"
	revision: "0.3"

class
	MENU_PRINCIPAL

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	MENU
		redefine
			make,
			run
		end


create
	make

feature {NONE} -- Constructeur

	make(a_window:GAME_WINDOW_RENDERED)
			-- Constructeur de `Current'
		do
			PRECURSOR {MENU} (a_window)
			create image.make (window.renderer, "menu.png")
		end

feature -- Routines

	run
			-- <Precursor>
		do
			PRECURSOR {MENU}
		end

	on_iteration(a_timestamp:NATURAL_32)
			-- <Precursor>
		do
			draw
		end

	on_mouse_pressed (a_timestamp: NATURAL_32; a_mouse_state: GAME_MOUSE_BUTTON_PRESSED_STATE; a_nb_clicks: NATURAL_8)
			-- <Precursor>
		do
			if a_mouse_state.is_left_button_pressed then
				if a_mouse_state.x >= 430 and a_mouse_state.x <= 935 then
					if a_mouse_state.y >= 150 and a_mouse_state.y <= 210 then
						is_starting := True
						game_library.stop
					end
					if a_mouse_state.y >= 230 and a_mouse_state.y <= 290 then

					end
					if a_mouse_state.y >= 310 and a_mouse_state.y <= 370 then
						is_menu_score:= True
						game_library.stop
					end
					if a_mouse_state.y >= 390 and a_mouse_state.y <= 450 then

					end
				end
			end
		end

	draw
		-- Dessine la sc�ne

		do
			window.renderer.draw_texture (image.texture, 0, 0)		-- Redessiner le background

			window.renderer.present		-- Met  jour les modifications dans l'�cran
		end

feature	-- Attributs

	is_menu_score:BOOLEAN
		-- Ce boolean d�termine si on acc�de au menu score ou non.

	is_starting:BOOLEAN
		-- Ce boolean d�termine si on commence le jeu ou non.

	image: IMAGE
		-- L'image du menu principal

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
