note
	description: "L'engin qui g�re le d�roulement du jeu."
	author: "Pier-Olivier Chagnon et C�dryk Coderre"
	date: "7 F�vrier 2018"
	revision: "0.5"

class
	ENGINE_GAME

inherit
	GAME_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED
	ENGINE

create
	make

feature {NONE} -- Constructeur

	make (a_window: GAME_WINDOW_RENDERED)
			-- Initialisation de `Current'
		require
			game_library_activer: game_library.is_video_enable
			audio_library_activer: audio_library.is_playback_enable
		do
			window := a_window
			create background.make (window.renderer)
			create {ARRAYED_LIST[BACKGROUND]} liste_level.make(8)
			create forest.make (window.renderer)
			create forest2.make (window.renderer)
			create cafe.make (window.renderer)
			create night.make (window.renderer)
			create park.make (window.renderer)
			create house.make (window.renderer)
			create town.make (window.renderer)
			create final.make (window.renderer)
			liste_level.extend (forest)
			liste_level.extend (forest2)
			liste_level.extend (night)
			liste_level.extend (house)
			liste_level.extend (park)
			liste_level.extend (cafe)
			liste_level.extend (town)
			liste_level.extend (final)
			create shaman.make (window.renderer)
			create musique.make
			create pointage.make
			create construire_pointage.make(window.renderer, pointage)
			position_level := 1
		end

feature -- Acc�s

	run
			-- <Precursor>
		local
			l_worker_points:WORKER_THREAD
		do
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (255, 128, 255))
			window.renderer.clear
			shaman.x := 10
			shaman.y := 402
			musique.play
			game_library.quit_signal_actions.extend (agent on_quit)
			window.key_pressed_actions.extend (agent on_key_pressed)
			window.key_released_actions.extend (agent on_key_released)
			game_library.iteration_actions.extend (agent on_iteration)
			create l_worker_points.make (agent pointage_thread)
			l_worker_points.launch
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
			l_worker_points.join
		end

	liste_level: LIST[BACKGROUND]
			-- Liste contenant les images des niveaux

	window:GAME_WINDOW_RENDERED
			-- La fen�tre du jeu

	shaman:SHAMAN
			-- Le personnage principal

	background:BACKGROUND
			-- Le fond d'�cran

	forest:FOREST
			-- Le fond d'�cran # 1

	forest2:FOREST2
			-- Le fond d'�cran # 2

	cafe:CAFE
			-- Le fond d'�cran # 6

	night:NIGHT
			-- Le fond d'�cran # 3

	park:PARK
			-- Le fond d'�cran # 5

	house:HOUSE
			-- Le fond d'�cran # 4

	town:TOWN
			-- Le fond d'�cran # 7

	final:FINAL
			-- Le fond d'�cran # 8

	musique:MUSIQUE
			-- La musique du jeu

	construire_pointage:FACTORY_POINTAGE
			-- Le texte de point pour le jeu

	is_menu_principal:BOOLEAN
			-- Pour savoir si le menu principal est appel�

	pointage:POINTAGE
			-- Calcul du pointage

	position_level:INTEGER
			-- La position des niveaux

	doit_quitter:BOOLEAN
			-- Boolean pour arr�ter un thread

	is_game_over: BOOLEAN
			-- Boolean pour savoir quand la partie se termine

feature {NONE} -- Impl�mentation

	on_iteration(a_timestamp:NATURAL_32)
			-- �v�nement qui est lanc� a chaque it�ration

		local
			l_position_x : INTEGER_32
			l_position_y : INTEGER_32
		do
			draw
			l_position_x := shaman.x
			l_position_y := shaman.y
			shaman.y := shaman.y + 15

			shaman.update (a_timestamp)	-- Mettre � jour les coordonn�es du personnage

			if shaman.x < 0 then		-- Pour s'assurer que le personnage ne sorte pas de l'�cran
				shaman.x := 0
				shaman.y := 402
			elseif shaman.x + shaman.sub_image_width > liste_level[position_level].texture.width then
				shaman.x := 0
				position_level := position_level + 1	-- Pour que l'image de background change lorsque le personnage arrive � l'extr�mit�
			elseif shaman.y < 0 then
				shaman.y := 0
			elseif shaman.y > 402 then
				shaman.y := 402
			elseif shaman.x + shaman.sub_image_width > liste_level[position_level].texture.width then
				shaman.x := liste_level[position_level].texture.width - shaman.sub_image_width
			elseif shaman.valide_collision (liste_level[position_level].plateformes.last) and position_level = 8 then
				is_game_over:= True
				game_library.stop
			end

			across liste_level[position_level].plateformes as plateforme
			loop
				if shaman.valide_collision (plateforme.item) then
					shaman.y := l_position_y
					shaman.x := l_position_x
				end
			end
		end


feature	-- Routines	

	draw
		-- Dessine la sc�ne

		do
			window.renderer.draw_texture (liste_level[position_level].texture, 0, 0)		-- Redessiner le background

			construire_pointage.build_score(pointage)

			window.renderer.draw_texture (construire_pointage.texture, 900, 0)

			window.renderer.draw_sub_texture_with_mirror (		-- Redessiner le personnage
									shaman.texture,  shaman.sub_image_x, shaman.sub_image_y, shaman.sub_image_width, shaman.sub_image_height,
									shaman.x, shaman.y, False, shaman.facing_left
								)

			across liste_level[position_level].plateformes
				 as plateforme
			loop
				window.renderer.draw_sub_texture (		-- Redessiner les plateformes
									plateforme.item.texture,  plateforme.item.sub_image_x, plateforme.item.sub_image_y,
									plateforme.item.sub_image_width, plateforme.item.sub_image_height, plateforme.item.x,
									plateforme.item.y
								)
			end

			window.renderer.present		-- Met � jour les modifications dans l'�cran
			audio_library.update
		end


feature -- Routines

	on_key_pressed(a_timestamp: NATURAL_32; a_key_state: GAME_KEY_STATE)
		-- Action qui se produit lorsqu'une touche est appuy�e

		do
			if not a_key_state.is_repeat then		-- Pour �tre certain que l'�v�nement ne soit pas une r�p�tition automatique de la touche
				if a_key_state.is_right then
					shaman.go_right(a_timestamp)
				elseif a_key_state.is_left then
					shaman.go_left(a_timestamp)
				elseif a_key_state.is_space and not shaman.is_jumping then
					shaman.jump (a_timestamp)
				end
			end
		end

	on_key_released(a_timestamp: NATURAL_32; a_key_state: GAME_KEY_STATE)
			-- Action lorsqu'une touche cesse d'etre appuy�e
		do
			if not a_key_state.is_repeat then
				if a_key_state.is_right then
					shaman.stop_right
				elseif a_key_state.is_left then
					shaman.stop_left
				end
			end
		end

	on_quit(a_timestamp: NATURAL_32)
			-- M�thode appel�e lorsque le signal de fermeture est appel�.
		do
			doit_quitter:= true
			game_library.stop  -- Arr�te la boucle du controlleur
		end

	pointage_thread
			-- Le thread du pointage
		do
			from
			until
				doit_quitter or is_game_over
			loop
				pointage.ajout_points (1)
				game_library.delay (10)
			end
		end


invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
