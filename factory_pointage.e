note
	description: "La classe {FACTORY_POINTAGE} permet de construire le score pour l'afficher"
	author: "C�dryk Coderre"
	date: "2018-04-17"
	revision: "0.2"

class
	FACTORY_POINTAGE

create
	make

feature {NONE} -- Constructeur

	make(a_renderer:GAME_RENDERER; pointage:POINTAGE)
			-- Permet d'initialiser la factory de pointages.
		require
			renderer_existant: a_renderer.exists
			No_Error: not has_error
		do
			renderer := a_renderer
			create font.make("font.ttf", 20)
			if font.is_openable then
				font.open
				has_error := not font.is_open
				build_score(pointage)
			else
				has_error := True
				create texture.make (a_renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
			end
		ensure
			verif_font: font.is_open or has_error
		end

feature -- Routines

	build_score(pointage:POINTAGE)
			-- Permet de construire le score pour l'afficher.
		require
			verif_font: font.is_open or has_error
		local
			l_text:TEXT_SURFACE_BLENDED
		do
			if font.is_open then
				create l_text.make ("score :" + pointage.points.out, font, create {GAME_COLOR}.make_rgb (0, 0, 0))
				create texture.make_from_surface (renderer, l_text)
			else
				create texture.make (renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
			end
		end


feature -- Attributs publique

	has_error:BOOLEAN
		-- Un bool�en permettant de voir si il y a eu une erreur dans `Current'

	texture:GAME_TEXTURE
		-- La texture utilis�e par `Current'

	font:TEXT_FONT
		-- Le font utilis� par `Current'


feature {NONE} -- Attributs priv�s

	renderer:GAME_RENDERER
		-- Le game_renderer utilis� par `Current'

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end

