note
	description: "Classe abstraite {ENGINE} dfinissant les fonctions basiques."
	author: "C�dryk Coderre"
	date: "2018-05-14"
	revision: "0.1"

deferred class
	ENGINE

feature -- Acc�s

	run
		-- Lance le jeu
		require
			No_Error: not has_error
		deferred
		end

	has_error:BOOLEAN
		-- Ce boolean permet de vrifier s'il y a eu une erreur ou non.

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
