note
	description: "Classe abstraite de {MENU} permettant de d�finir les fonctions basiques des menus."
	author: "C�dryk Coderre et Pier-Olivier Chagnon"
	date: "2018-05-14"
	revision: "0.3"

deferred class
	MENU

inherit
	GAME_LIBRARY_SHARED
	ENGINE

feature {NONE} -- Constructeur

	make(a_window:GAME_WINDOW_RENDERED)
			-- Constructeur de `Current'
		require
			game_library_activer: game_library.is_video_enable
		do
			window := a_window
		end

feature -- Routines

	run
			-- Permet de commencer les �venements
		do
			window.renderer.clear
			game_library.quit_signal_actions.extend (agent on_quit)
			game_library.iteration_actions.extend (agent on_iteration)
			window.mouse_button_pressed_actions.extend (agent on_mouse_pressed)	-- Quand le bouton d'une souris est appuy�.
			if window.renderer.driver.is_present_synchronized_supported then
				game_library.launch_no_delay
			else
				game_library.launch
			end
		end

	on_quit(a_timestamp:NATURAL_32)
			-- Quand l'utilisateur ferme le jeu.
		do
			game_library.stop
		end

	on_mouse_pressed (a_timestamp: NATURAL_32; a_mouse_state: GAME_MOUSE_BUTTON_PRESSED_STATE; a_nb_clicks: NATURAL_8)
			-- Permet la s�lection de menu
		deferred
		end

	on_iteration(a_timestamp:NATURAL_32)
			-- Red�sinne la sc�ne � chaque it�ration
		deferred
		end

feature -- Attributs

	window:GAME_WINDOW_RENDERED
		-- Le window du jeu.

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end




