note
	description: "[
		Tests des classes {POINTAGE}, {POINTS}, {MUSIQUE} et {SON}.
	]"
	author: "C�dryk Coderre"
	date: "2018-04-18"
	revision: "0.2"
	testing: "type/manual"

class
	TEST_POINTS_MUSIQUE_SON

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end



feature {NONE}

	on_prepare
			-- Permet de pr�parer les tests.
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			audio_library.enable_playback
			text_library.enable_text
			create l_window_builder
			window := l_window_builder.generate_window
		end

	on_clean
			-- Permet de nettoyer les tests.
		do
			text_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

	window:GAME_WINDOW_RENDERED
		-- Le window utilis� pour les tests.

feature -- Test routines

	test_pointage_ajouter
			-- Test normal de ajout_points {POINTAGE}. `ajout_points'
		note
			testing:  "execution/isolated"
		local
			l_pointage:POINTAGE
		do
			create l_pointage.make
			l_pointage.ajout_points(5)

			assert ("Pointage invalide",  l_pointage.points = 5)
		end

	test_pointage_enlever
			-- Test normal de enlever_points {POINTAGE}. `enlever_points'
		note
			testing:  "execution/isolated"
		local
			l_pointage:POINTAGE
		do
			create l_pointage.make
			l_pointage.ajout_points(15)
			l_pointage.enlever_points(10)

			assert ("Pointage invalide", l_pointage.points.is_equal (5))
		end

	test_pointage_nom
			-- Test normal de nom {POINTAGE}. `nom'
		note
			testing:  "execution/isolated"
		local
			l_pointage:POINTAGE
			l_string:READABLE_STRING_GENERAL
		do
			create l_pointage.make
			l_pointage.set_nom ("johnny")
			l_string := "johnny"

			assert ("Pointage invalide", l_pointage.nom.is_equal (l_string))
		end

	test_pointage_set_points
			-- Test normal de set_points {POINTAGE}. `set_points'
		note
			testing:  "execution/isolated"
		local
			l_pointage:POINTAGE
		do
			create l_pointage.make
			l_pointage.set_points (20)

			assert ("Pointage invalide", l_pointage.points = 20)
		end

	test_json
			-- Test normal de deserialization {HTTP_HELPER}. `deserialization'
		note
			testing: "execution/isolated"
		local
			l_file:PLAIN_TEXT_FILE
			l_text:STRING
			l_http_helper:HTTP_HELPER
			l_liste_pointage:LIST[POINTAGE]
		do
			create l_http_helper.make
			from
				l_text := ""
				create l_file.make_open_read ("tests/json.txt")
			until
				l_file.end_of_file
			loop
				l_file.read_line
				l_text.append (l_file.last_string)
			end

			l_liste_pointage := l_http_helper.deserialization (l_text)

			assert("JSON invalide", True)
		end

	test_pointage_constructeur
			-- Test normal du constructeur {POINTAGE}. `make'
		note
			testing:  "execution/isolated"
		local
			l_pointage:POINTAGE
		do
			create l_pointage.make

			assert("Pointage impossible", true);
		end

	test_pointage_negatif
			-- Test n�gatif de ajout_points {POINTAGE}. `ajout_points'
		note
			testing:  "execution/isolated"
		local
			l_pointage:POINTAGE
		do
			create l_pointage.make
			l_pointage.ajout_points(-5)

			assert ("Pointage negatif", false)
		end

	test_points_constructeur
			-- Test normal du constructeur de {FACTORY_POINTAGE}.  `make'
		note
			testing:  "execution/isolated"
		local
			l_construire_pointage:FACTORY_POINTAGE
			l_pointage:POINTAGE
		do
			create l_pointage.make
			create l_construire_pointage.make(window.renderer, l_pointage)

			assert("Points invalide", true);
		end

	test_points_routine
			-- Test normal de la routine build_score de {FACTORY_POINTAGE}.  `build_score'
		note
			testing:  "execution/isolated"
		local
			l_construire_pointage:FACTORY_POINTAGE
			l_pointage:POINTAGE
		do
			create l_pointage.make
			create l_construire_pointage.make(window.renderer, l_pointage)
			l_construire_pointage.build_score(l_pointage)

			assert("build_score invalide", true);
		end

	test_musique_constructeur
			-- Test normal du constructeur de {MUSIQUE}. `make'
		note
			testing:  "execution/isolated"
		local
			l_musique:MUSIQUE
		do
			create l_musique.make

			assert("Musique invalide", true);
		end

	test_son_constructeur
			-- Test normal du constructeur de {SON}. `make'
		note
			testing:  "execution/isolated"
		local
			l_son:SON
		do
			create l_son.make("saut.wav")

			assert("Son invalide", true);
		end


	test_son_inexistant
			-- Test avec chemin invalide du constructeur de {SON}. `make'
		note
			testing:  "execution/isolated"
		local
			l_son:SON
		do
			create l_son.make("trecytrveutrebutynbiuytnuytbutybtuyb")

			assert("Son invalide", l_son.has_error = true)
		end

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
