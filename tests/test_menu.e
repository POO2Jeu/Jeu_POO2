note
	description: "[
		Tests des classes {MENU_PRINCIPAL}, {ROUTEUR}, {MENU_SCORE} et {GAME_OVER}.
	]"
	author: "C�dryk Coderre"
	date: "2018-04-18"
	revision: "0.2"
	testing: "type/manual"

class
	TEST_MENU

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end
	AUDIO_LIBRARY_SHARED
		undefine
			default_create
		end
	TEXT_LIBRARY_SHARED
		undefine
			default_create
		end
	ROUTEUR
		undefine
			default_create
		end




feature {NONE} -- Initialisation des tests

	on_prepare
			-- Permet de pr�parer les tests.
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			audio_library.enable_playback
			text_library.enable_text
			make
		end

	on_clean
			-- Permet de nettoyer les tests.
		do
			text_library.quit_library
			audio_library.quit_library
			game_library.quit_library
		end

feature -- Test routines

	test_routeur_constructeur
			-- Test normal du constructeur de {ROUTEUR}. `make'
		note
			testing:  "execution/isolated"
		local
			l_routeur:ROUTEUR
		do
			create l_routeur.make

			assert("Routeur invalide", true);
		end

	test_routeur_windows
			-- Test normal qui v�rifie les dimensions de windows de la classe {ROUTEUR}. `make'
		note
			testing:  "execution/isolated"
		local
			l_routeur:ROUTEUR
		do
			create l_routeur.make

			assert("Routeur invalide", l_routeur.window.width = 1024 and l_routeur.window.height = 512);
		end

	test_routeur_start_game
			-- Test �ronn� de start_game de {ROUTEUR}. `start_game'
		note
			testing:  "execution/isolated"
		do
			start_game

			assert("Routeur start_game invalide", False);
		end

	test_routeur_menu_principal
			-- Test �ronn� de menu_principal de {ROUTEUR}. `menu_principal'
		note
			testing:  "execution/isolated"
		do
			is_menu_principal := False
			menu_principal

			assert("Routeur menu_principal invalide", False);
		end

	test_routeur_menu_score
			-- Test �ronn� de menu_score de {ROUTEUR}. `menu_score'
		note
			testing:  "execution/isolated"
		do
			menu_score

			assert("Routeur menu_score invalide", False);
		end

	test_routeur_game_over
			-- Test �ronn� de game_over de {ROUTEUR}. `game_over'
		note
			testing:  "execution/isolated"
		do
			game_over

			assert("Routeur game_over invalide", False);
		end

	test_menu_principal_constructeur
			-- Test normal de make de {MENU_PRINCIPAL}. `make'
		note
			testing:  "execution/isolated"
		local
			l_menu_principal:MENU_PRINCIPAL
		do
			create l_menu_principal.make (window)


			assert("Menu principal invalide", true);
		end

	test_menu_score_constructeur
			-- Test normal de make de {MENU_SCORE}. `make'
		note
			testing:  "execution/isolated"
		local
			l_menu_score:MENU_SCORE
		do
			create l_menu_score.make (window)


			assert("Menu score invalide", true);
		end

	test_game_over_constructeur
			-- Test normal de make de {GAME_OVER}. `make'
		note
			testing:  "execution/isolated"
		local
			l_game_over:GAME_OVER
		do
			create l_game_over.make (window)


			assert("Game over invalide", true);
		end

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
