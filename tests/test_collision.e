note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	TEST_COLLISION

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE} -- Events

	on_prepare
			-- <Precursor>
		do

		end

	on_clean
			-- <Precursor>
		do

		end

feature -- Test routines

	test_collision_valeur_normale
			-- Test avec valeurs normales de {COLLISION}.
		note
			testing:  "execution/isolated"
		local
			l_objet_1: COLLISION
			l_objet_2: COLLISION
		do
			create l_objet_1
			create l_objet_2
			l_objet_1.x := 10
			l_objet_1.y := 10
			l_objet_2.x := 20
			l_objet_2.y := 20

			if(
				l_objet_1.x >= l_objet_2.x
				and
				l_objet_1.x <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y >= l_objet_2.y
				and
				l_objet_1.y <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x + l_objet_1.sub_image_width >= l_objet_2.x
				and
				l_objet_1.x + l_objet_1.sub_image_width <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y >= l_objet_2.y
				and
				l_objet_1.y <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x >= l_objet_2.x
				and
				l_objet_1.x <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y + l_objet_1.sub_image_height >= l_objet_2.y
				and
				l_objet_1.y + l_objet_1.sub_image_height <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x + l_objet_1.sub_image_width >= l_objet_2.x
				and
				l_objet_1.x + l_objet_1.sub_image_width <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y + l_objet_1.sub_image_height >= l_objet_2.y
				and
				l_objet_1.y + l_objet_1.sub_image_height <= l_objet_2.y + l_objet_2.sub_image_height
				) then
			end
		end


	test_collision_valeur_negative
			-- Test avec valeurs n�gatives de {COLLISION}.
		note
			testing:  "execution/isolated"
		local
			l_objet_1: COLLISION
			l_objet_2: COLLISION
		do
			create l_objet_1
			create l_objet_2
			l_objet_1.x := -10
			l_objet_1.y := -10
			l_objet_2.x := -10
			l_objet_2.y := -10

			if(
				l_objet_1.x >= l_objet_2.x
				and
				l_objet_1.x <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y >= l_objet_2.y
				and
				l_objet_1.y <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x + l_objet_1.sub_image_width >= l_objet_2.x
				and
				l_objet_1.x + l_objet_1.sub_image_width <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y >= l_objet_2.y
				and
				l_objet_1.y <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x >= l_objet_2.x
				and
				l_objet_1.x <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y + l_objet_1.sub_image_height >= l_objet_2.y
				and
				l_objet_1.y + l_objet_1.sub_image_height <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x + l_objet_1.sub_image_width >= l_objet_2.x
				and
				l_objet_1.x + l_objet_1.sub_image_width <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y + l_objet_1.sub_image_height >= l_objet_2.y
				and
				l_objet_1.y + l_objet_1.sub_image_height <= l_objet_2.y + l_objet_2.sub_image_height
				) then
			end
			assert ("Valeur n�gative", false)
		end


		test_collision_valeur_limite
			-- Test avec valeurs limites de {COLLISION}.
		note
			testing:  "execution/isolated"
		local
			l_objet_1: COLLISION
			l_objet_2: COLLISION
		do
			create l_objet_1
			create l_objet_2
			l_objet_1.x := 0
			l_objet_1.y := 0
			l_objet_2.x := 0
			l_objet_2.y := 0

			if(
				l_objet_1.x >= l_objet_2.x
				and
				l_objet_1.x <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y >= l_objet_2.y
				and
				l_objet_1.y <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x + l_objet_1.sub_image_width >= l_objet_2.x
				and
				l_objet_1.x + l_objet_1.sub_image_width <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y >= l_objet_2.y
				and
				l_objet_1.y <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x >= l_objet_2.x
				and
				l_objet_1.x <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y + l_objet_1.sub_image_height >= l_objet_2.y
				and
				l_objet_1.y + l_objet_1.sub_image_height <= l_objet_2.y + l_objet_2.sub_image_height
				) or (
				l_objet_1.x + l_objet_1.sub_image_width >= l_objet_2.x
				and
				l_objet_1.x + l_objet_1.sub_image_width <= l_objet_2.x + l_objet_2.sub_image_width
				and
				l_objet_1.y + l_objet_1.sub_image_height >= l_objet_2.y
				and
				l_objet_1.y + l_objet_1.sub_image_height <= l_objet_2.y + l_objet_2.sub_image_height
				) then
			end
			assert ("Valeur limite", false)
		end

	x:INTEGER assign set_x
			-- Position verticale de `Current'

	y:INTEGER assign set_y
			-- Position horizontale de `Current'

	set_x(a_x:INTEGER)
			-- Assigne la valeur de `x' � `a_x'
		do
			x := a_x
		ensure
			Is_Assign: x = a_x
		end

	set_y(a_y:INTEGER)
			-- Assigne la valeur de `y' � `a_y'
		do
			y := a_y
		ensure
			Is_Assign: y = a_y
		end

	sub_image_x, sub_image_y:INTEGER
			-- Position de la portion de l'image dans `surface'

	sub_image_width: INTEGER
			-- Dimension de la portion de l'image dans `surface'

	sub_image_height: INTEGER
			-- Dimension de la portion de l'image dans `surface'

end


