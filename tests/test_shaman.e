note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	TEST_SHAMAN

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	DOUBLE_MATH
		undefine
			default_create
		end

feature{NONE}

	on_prepare
		do

		end

	on_clean
		do

		end

feature -- Test routines

	test_jump_shaman
			-- Test du jump normal de {SHAMAN}.
		note
			testing: "execution/isolated"
		local
			l_timestamp:NATURAL_32
			l_delta_time:NATURAL_32
			l_is_jumping:BOOLEAN
			l_jump_time:NATURAL_32
			l_jump_timestamp:NATURAL_32
			l_y:INTEGER_32
			l_jump_y:INTEGER_32
			l_jump_height: INTEGER
		do
			l_y:= 0
			l_is_jumping:= True
			l_jump_height:= 150
			l_jump_time:= 600
			if l_is_jumping then
				l_delta_time := l_timestamp - l_jump_timestamp
				l_y := l_jump_y - (l_jump_height.to_double * sine((l_delta_time / l_jump_time) * pi)).rounded
				if l_delta_time > l_jump_time then
				l_is_jumping := False
			end
		end
	end

	test_jump_negatif_shaman
			-- Test du jump n�gatif de {SHAMAN}.
		note
			testing: "execution/isolated"
		local
			l_timestamp:NATURAL_32
			l_delta_time:NATURAL_32
			l_is_jumping:BOOLEAN
			l_jump_time:NATURAL_32
			l_jump_timestamp:NATURAL_32
			l_y:INTEGER_32
			l_jump_y:INTEGER_32
			l_jump_height: INTEGER
		do
			l_y:= 0
			l_is_jumping:= True
			l_jump_height:= -150
			l_jump_time:= 600
			if l_is_jumping then
				l_delta_time := l_timestamp - l_jump_timestamp
				l_y := l_jump_y - (l_jump_height.to_double * sine((l_delta_time / l_jump_time) * pi)).rounded
				if l_delta_time > l_jump_time then
				l_is_jumping := False
			end
		end
		assert ("Valeur n�gative", false)
	end

	test_jump_limite_shaman
			-- Test du jump limite de {SHAMAN}.
		note
			testing: "execution/isolated"
		local
			l_timestamp:NATURAL_32
			l_delta_time:NATURAL_32
			l_is_jumping:BOOLEAN
			l_jump_time:NATURAL_32
			l_jump_timestamp:NATURAL_32
			l_y:INTEGER_32
			l_jump_y:INTEGER_32
			l_jump_height: INTEGER
		do
			l_y:= 0
			l_is_jumping:= True
			l_jump_height:= 512
			l_jump_time:= 600
			if l_is_jumping then
				l_delta_time := l_timestamp - l_jump_timestamp
				l_y := l_jump_y - (l_jump_height.to_double * sine((l_delta_time / l_jump_time) * pi)).rounded
				if l_delta_time > l_jump_time then
				l_is_jumping := False
			end
		end
		assert ("Valeur limite", false)
	end

	test_movement_shaman
			-- Test de mouvement normal de {SHAMAN}.
		note
			testing: "execution/isolated"
		local
			l_timestamp:NATURAL_32
			l_delta_time:NATURAL_32
			l_going_right:BOOLEAN
			l_going_left:BOOLEAN
			l_facing_left:BOOLEAN
			l_old_timestamp:NATURAL_32
			l_movement_delta:NATURAL_32
			l_animation_delta:NATURAL_32
			l_x:INTEGER_32

		do
			l_animation_delta:= 100
			l_delta_time := l_timestamp - l_old_timestamp
			l_movement_delta:= 8
			if l_delta_time // l_movement_delta > 0 then
				if l_going_right then
					l_facing_left := False
					l_x := l_x + (l_movement_delta).to_integer_32
				elseif l_going_left then
					l_facing_left := True
					l_x := l_x - (l_movement_delta).to_integer_32
				end
			end
	end

	test_movement_limite_shaman
			-- Test de mouvement limite de {SHAMAN}.
		note
			testing: "execution/isolated"
		local
			l_timestamp:NATURAL_32
			l_delta_time:NATURAL_32
			l_going_right:BOOLEAN
			l_going_left:BOOLEAN
			l_facing_left:BOOLEAN
			l_old_timestamp:NATURAL_32
			l_movement_delta:NATURAL_32
			l_animation_delta:NATURAL_32
			l_x:INTEGER_32

		do
			l_animation_delta:= 10000
			l_delta_time := l_timestamp - l_old_timestamp
			l_movement_delta:= 10000
			if l_delta_time // l_movement_delta > 0 then
				if l_going_right then
					l_facing_left := False
					l_x := l_x + (l_movement_delta).to_integer_32
				elseif l_going_left then
					l_facing_left := True
					l_x := l_x - (l_movement_delta).to_integer_32
				end
			end
		assert ("Valeur limite", false)
	end
end

