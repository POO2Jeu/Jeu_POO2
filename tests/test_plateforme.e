note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	TEST_PLATEFORME

inherit
	EQA_TEST_SET
		redefine
			on_prepare,
			on_clean
		end
	GAME_LIBRARY_SHARED
		undefine
			default_create
		end

feature {NONE}

	on_prepare
			-- Permet de pr�parer les tests.
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			game_library.enable_video
			create l_window_builder
			l_window_builder.set_dimension (1024, 512)
			window := l_window_builder.generate_window
		end

	on_clean
			-- Permet de nettoyer les tests.
		do
			game_library.quit_library
		end

	window:GAME_WINDOW_RENDERED

feature -- Test routines

	test_creation_plateforme
			-- Cr�ation normale de {PLATEFORME}.
		note
			testing: "execution/isolated"
		local
			l_background:BACKGROUND
			l_plateforme: PLATEFORME
		do
			create l_background.make (window.renderer)
			create l_plateforme.make (window.renderer, 10, 10, 10, 10)
		end

	test_creation_plateforme_valeur_negative
			-- Cr�ation normale de {PLATEFORME} avec valeur n�gative.
		note
			testing: "execution/isolated"
		local
			l_background:BACKGROUND
			l_plateforme: PLATEFORME
		do
			create l_background.make (window.renderer)
			create l_plateforme.make (window.renderer, -10, -10, -10, -10)
			assert ("Valeur n�gative", false)
		end

	test_creation_plateforme_valeur_limite
			-- Cr�ation normale de {PLATEFORME} avec valeur limite.
		note
			testing: "execution/isolated"
		local
			l_background:BACKGROUND
			l_plateforme: PLATEFORME
		do
			create l_background.make (window.renderer)
			create l_plateforme.make (window.renderer, 0, 0, 0, 0)
			assert ("Valeur limite", false)
		end
end


