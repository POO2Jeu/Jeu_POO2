note
	description: "La classe {ROUTEUR} permet de diriger l'utilisateur vers les diff�rents engins."
	author: "C�dryk Coderre"
	date: "2018-05-14"
	revision: "0.2"

class
	ROUTEUR

inherit
	GAME_LIBRARY_SHARED
	ENGINE

create
	make

feature {NONE} -- Constructeur

	make
			-- Constructeur de la classe {ROUTEUR}
		local
			l_window_builder:GAME_WINDOW_RENDERED_BUILDER
		do
			create l_window_builder
			l_window_builder.set_dimension (1024, 512)
			l_window_builder.set_title ("Savage 3: The Tortured Students")
			l_window_builder.enable_must_renderer_synchronize_update	-- Ask to the video card to manage the frame synchronisation (FPS)
			window := l_window_builder.generate_window
			create engine.make(window)
		end

feature

	run
			-- Lance le jeu
		do
			from
				is_menu_principal := True
			until
				not (is_engine or is_menu_principal or is_menu_score or is_game_over)
			loop
				if is_engine then
					start_game
				elseif is_menu_principal then
					menu_principal
				elseif is_menu_score then
					menu_score
				elseif is_game_over then
					game_over
				end
				game_library.clear_all_events
			end
		end

	start_game
			-- D�marre le jeu
		require
			routeur_game_vrai: is_engine
		do
			is_engine := False
			create engine.make (window)
			start_engine(engine)
			if engine.is_game_over then
				is_game_over := True
			end
		ensure
			routeur_game_vrai: not is_engine
		end

	start_engine(a_engine:ENGINE)
			-- D�marre l'engin sp�cifi�.
		do
			if not a_engine.has_error then
				a_engine.run
			end
		end

	menu_principal
			-- D�marre le menu principal.
		require
			routeur_menu_principal_vrai: is_menu_principal
		local
			l_menu_principal:MENU_PRINCIPAL
		do
			is_menu_principal := False
			create l_menu_principal.make(window)
			start_engine(l_menu_principal)
			if l_menu_principal.is_starting then
				is_engine := True
			elseif l_menu_principal.is_menu_score then
				is_menu_score := True
			end
		ensure
			routeur_menu_principal_faux: not is_menu_principal
		end

	menu_score
			-- D�marre le menu score.
		require
			routeur_menu_score_vrai: is_menu_score
		local
			l_menu_score:MENU_SCORE
		do
			is_menu_score := False
			create l_menu_score.make(window)
			start_engine(l_menu_score)
			if l_menu_score.is_menu_principal then
				is_menu_principal := True
			end
		ensure
			routeur_menu_score_faux: not is_menu_score
		end

	game_over
			-- D�marre l'�cran Game Over.
		require
			routeur_game_over_vrai: is_game_over
		local
			l_http_helper: HTTP_HELPER
			l_game_over:GAME_OVER
		do
			is_game_over := False
			create l_http_helper.make
			create l_game_over.make(window)
			l_game_over.set_pointage (engine.pointage)
			start_engine(l_game_over)
			if l_game_over.is_menu_principal then
				if not l_game_over.pointage.nom.is_empty then
					if not l_http_helper.has_error then
						l_http_helper.post_pointage (l_game_over.pointage)
					end
				end
				is_menu_principal := True
			end
		ensure
			routeur_game_over_faux: not is_game_over
		end


feature -- Attributs

	window:GAME_WINDOW_RENDERED
		-- Fen�tre du jeu.

	is_engine, is_menu_principal, is_menu_score, is_game_over:BOOLEAN
		-- Boolean pour naviguer dans les menus.

	engine:ENGINE_GAME
		-- Moteur du jeu.


invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end

