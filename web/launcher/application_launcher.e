note
	description: "[
				Effective class for APPLICATION_LAUNCHER_I

				You can put modification in this class
			]"
	date: "$Date: 2015-06-12 03:59:28 -0700 (Fri, 12 Jun 2015) $"
	revision: "$Revision: 97466 $"

class
	APPLICATION_LAUNCHER [G -> WSF_EXECUTION create make end]

inherit
	APPLICATION_LAUNCHER_I [G]

feature -- Custom

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
