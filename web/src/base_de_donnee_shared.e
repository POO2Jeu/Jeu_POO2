note
	description: "Permet de faire une seule connection � la base de donn�e avec la classe {BASE_DE_DONNEE_SHARED}."
	author: "C�dryk Coderre"
	date: "2018-05-02"
	revision: "0.1"

deferred class
	BASE_DE_DONNEE_SHARED

feature

	base_de_donnee:BASE_DE_DONNEE
			-- Permet de faire une seule connection � la base
			-- de donn�e.
		once ("PROCESS")
			create Result.make
		end
		
invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end

