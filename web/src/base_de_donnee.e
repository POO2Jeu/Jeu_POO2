note
	description: "La classe {BASE_DE_DONNEE} permet de g�rer une base de donn�e sqlite."
	author: "C�dryk Coderre"
	date: "2018-05-02"
	revision: "0.2"

class
	BASE_DE_DONNEE

create
	make

feature {NONE} -- Initialisation

	make
		-- Constructeur
		do
			ouvre_base_de_donnee
		end

	ouvre_base_de_donnee
			-- Ouvre la base de donn�e et cr�er la base de donn�e,
			-- si elle n'existe pas.
		local
			l_repository_factory:PS_SQLITE_REPOSITORY_FACTORY
			l_fichier:RAW_FILE
			l_nom_fichier:STRING
			l_nouveau_fichier:BOOLEAN
		do
			l_nom_fichier := "database.sqlite"
			create l_fichier.make_with_name (l_nom_fichier)
			l_nouveau_fichier := not l_fichier.exists
			create l_repository_factory.make
			l_repository_factory.set_database (l_nom_fichier)
			repository := l_repository_factory.new_repository
			if l_nouveau_fichier then
				initialiser_base_de_donnee
			end
		ensure
			repository_exists: attached repository or a_erreur
		end

	initialiser_base_de_donnee
			-- Permet d'initialiser la base de donn�e avec 10 donn�es de tests.
		require
			repository_exists: attached repository
		local
			l_pointage:POINTAGE
			l_transaction:PS_TRANSACTION
		do
			l_transaction := repository.new_transaction
			if not l_transaction.has_error then
				across 1 |..| 10 as la_index loop
					create l_pointage.default_create
					l_pointage.set_nom ("Utilisateur" + la_index.item.out)
					l_pointage.set_points (500 - la_index.item)
					l_transaction.insert (l_pointage)
				end
				l_transaction.commit
			end
			a_erreur := l_transaction.has_error
		end

feature -- Acc�s

	repository:PS_REPOSITORY
			-- Le "repository" de la base de donn�e

	a_erreur:BOOLEAN
			-- Une erreur est survenue lors de la cr�ation de `Current'

	affiche_pointages: LIST [POINTAGE]
			-- Retourne tous les {POINTAGE} de la base de donn�e
			-- dans une liste class�
		local
			l_query:PS_QUERY[POINTAGE]
			l_criterion_factory:PS_CRITERION_FACTORY
			l_transaction:PS_TRANSACTION
			l_cursor:ITERATION_CURSOR[POINTAGE]
			l_pointage:POINTAGE
			l_liste_pointage: SORTED_TWO_WAY_LIST [POINTAGE]
		do
			create l_criterion_factory
			create l_query.make

			l_transaction := repository.new_transaction
			if not l_transaction.has_error then
				l_transaction.execute_query (l_query)
			end

			create l_liste_pointage.make
			from
				l_cursor := l_query.new_cursor
			until
				l_cursor.after
			loop
				l_pointage := l_cursor.item
				l_liste_pointage.extend (l_pointage)
				l_cursor.forth
			end
			Result := l_liste_pointage
		end

	ajouter_pointage_bd(a_pointage:POINTAGE)
			-- Permet d'ajouter un entr� � la base de donn�e.
		require
			repository_exists: attached repository
		local
			l_transaction:PS_TRANSACTION
		do
			l_transaction := repository.new_transaction
			if not l_transaction.has_error then
				l_transaction.insert (a_pointage)
				l_transaction.commit
			end
			a_erreur := l_transaction.has_error
		end

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
