note
	description: "La classe {CONVERTIR_POINTAGE} permet de convertir une liste en string."
	author: "C�dryk Coderre"
	date: "2018-05-02"
	revision: "0.2"

class
	CONVERTIR_POINTAGE

feature -- Builder		

	convertir(a_liste_pointage:LIST[POINTAGE]): STRING
			-- Permet de convertir une liste de pointage en JSON.
		local
			l_factory: JSON_SERIALIZATION_FACTORY
			l_convert: JSON_SERIALIZATION
			l_erreur: STRING
		do
			l_convert := l_factory.smart_serialization

			l_convert.set_pretty_printing
			if attached l_convert.to_json_string (a_liste_pointage) as l_pointage_json then
				Result := l_pointage_json
			else
				l_erreur := "erreur"
				Result := l_convert.to_json_string(l_erreur)
			end

		end

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
