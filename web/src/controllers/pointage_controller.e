note
	description: "La classe {POINTAGE_CONTROLLER} contient les controlleurs pour les pointages."
	author: "C�dryk Coderre"
	date: "2018-05-02"
	revision: "0.2"

class
	POINTAGE_CONTROLLER

inherit
	WSF_URI_TEMPLATE_RESPONSE_HANDLER
	BASE_DE_DONNEE_SHARED


feature -- Access

	response (a_request: WSF_REQUEST): WSF_RESPONSE_MESSAGE
			-- G�n�re une page pour le `request_type' re�u de `a_request`
		local
			l_page:WSF_PAGE_RESPONSE
		do
			if a_request.is_request_method ("GET") then
				l_page := get_pointage
			elseif a_request.is_request_method ("POST") then
				l_page := post_pointage(a_request)
			else
				l_page := retour_erreur
			end
			Result := l_page
		end

	get_pointage: WSF_PAGE_RESPONSE
			-- Permet d'obtenir les pointages lorsqu'on re�oit un GET.
		local
			l_page:WSF_PAGE_RESPONSE
			l_helper:CONVERTIR_POINTAGE
			l_liste_pointage:LIST[POINTAGE]
		do
			create l_helper
			l_liste_pointage := base_de_donnee.affiche_pointages
			if attached l_helper.convertir(l_liste_pointage) as la_pointage then
				create l_page.make_with_body (la_pointage)
				l_page.header.add_content_type ("application/json")
				l_page.set_status_code (200)
				Result := l_page
			else
				Result := retour_erreur
			end
		end

	post_pointage(a_request: WSF_REQUEST): WSF_PAGE_RESPONSE
			-- Permet d'ajouter un pointage dans la base de donn�e avec un POST.
		local
			l_page:WSF_PAGE_RESPONSE
			l_pointage:POINTAGE
		do
			create l_pointage.default_create
			if attached a_request.item ("nom") as la_nom then
				l_pointage.set_nom (la_nom.string_representation)
			end
			if attached a_request.item ("points") as la_points then
				if la_points.string_representation.is_integer_64 then
					l_pointage.set_points (la_points.string_representation.to_integer_64)
				else
					Result := retour_erreur
				end
			end
			base_de_donnee.ajouter_pointage_bd (l_pointage)
			create l_page.make
			l_page.set_status_code (201)
			Result := l_page
		end

	retour_erreur: WSF_PAGE_RESPONSE
			-- Erreur 500 retourn� en cas d'erreur.
		local
			l_page:WSF_PAGE_RESPONSE
		do
			create l_page.make
			l_page.set_status_code (500)
			Result := l_page
		end

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
