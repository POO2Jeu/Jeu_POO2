note
	description: "La classe {POINTAGE} est un mod�le permettant de g�rer les pointages."
	author: "C�dryk Coderre"
	date: "2018-05-02"
	revision: "0.2"

class
	POINTAGE

inherit
	COMPARABLE
		redefine
			default_create
		end

create
	default_create

feature {NONE} -- Initialisation

	default_create
			-- Constructeur
		do
			set_nom("")
			set_points(0)
		end

feature -- Acc�s

	nom: READABLE_STRING_GENERAL
			-- Nom de celui qui a soumis son pointage.

	points: INTEGER_64
			-- Le nombre de points

feature -- M�thodes

	set_nom (a_nom: READABLE_STRING_GENERAL)
			-- Assigne `nom' avec la valeur de `a_nom'
		require
			valeur_existe: a_nom /= Void
		do
			nom := a_nom
		ensure
			nom_set: a_nom.same_string(nom)
		end

	set_points (a_points: INTEGER_64)
			-- Assigne `point' avec la valeur de `a_point'
		do
			points := a_points
		ensure
			points_set: a_points = points
		end

feature -- Comparison

	is_less alias "<" (a_other: like Current): BOOLEAN
			-- <Precursor>
		do
			Result := points < a_other.points
		end
invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
