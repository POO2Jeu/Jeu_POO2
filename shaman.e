 note
	description: "Personnage principal du jeu et ses mouvements."
	author: "Pier-Olivier Chagnon"
	date: "7 F�vrier 2018"
	revision: "1.0"

class
	SHAMAN

inherit
	DOUBLE_MATH
	SON
		rename
			make as make_son
		end
	COLLISION
	IMAGE
		rename
			make as make_image
		end

create
	make

feature {NONE} -- Constructeur

	make(a_renderer:GAME_RENDERER)
			-- Initialisation de `Current' avec `a_renderer'
		require
			a_renderer.has_error = false
		do
			make_son("saut.wav")
			make_image(a_renderer, "slime.png")
			sub_image_width := 72
			sub_image_height := 75
			initialize_animation_coordinate
		end

	initialize_animation_coordinate
			-- Cr�� les `animation_coordinates'

		do
			create {ARRAYED_LIST[TUPLE[x,y:INTEGER]]} animation_coordinates.make(4)
			animation_coordinates.extend ([(72), 0])
			animation_coordinates.extend ([0, 0])
			animation_coordinates.extend ([(72) * 2, 0])
			animation_coordinates.extend ([0, 0])

			sub_image_x := animation_coordinates.first.x	-- Place l'image sans mouvement
			sub_image_y := animation_coordinates.first.y	-- Place l'image sans mouvement
		end

feature -- Acc�s

	update(a_timestamp:NATURAL_32)
			-- Met la surface � jour selon le `a_timestamp'.
			-- Chaque 100 ms, l'image changee; chaque 8ms `Current' bouge
		local
			l_coordinate:TUPLE[x,y:INTEGER]
			l_delta_time:NATURAL_32
		do
			if going_left or going_right or is_jumping then
				l_coordinate := animation_coordinates.at ((((a_timestamp // animation_delta) \\
										animation_coordinates.count.to_natural_32) + 1).to_integer_32)

				sub_image_x := l_coordinate.x
				sub_image_y := l_coordinate.y
				l_delta_time := a_timestamp - old_timestamp

				if l_delta_time // movement_delta > 0 then
					if going_right then
						facing_left := False
						x := x + (movement_delta).to_integer_32		-- Fait bouger le personnage vers la droite
					elseif going_left then
						facing_left := True
						x := x - (movement_delta).to_integer_32		-- Fait bouger le personnage vers la gauche
					end
					if is_jumping then
						l_delta_time := a_timestamp - jump_timestamp
						y := jump_y - (jump_height.to_double * sine((l_delta_time / jump_time) * pi)).rounded
						if l_delta_time > jump_time then
						is_jumping := False
						end
					end
				old_timestamp := old_timestamp + (l_delta_time // movement_delta) * movement_delta
			end
		end
	end

	go_left(a_timestamp:NATURAL_32)
			-- Fait `Current' bouger � gauche
		require
			is_not_going_left: going_left = False
		do
			old_timestamp := a_timestamp
			going_left := True
		end

	go_right(a_timestamp:NATURAL_32)
			-- Fait `Current' bouger � droite
		require
			is_not_going_right: going_right = False
		do
			old_timestamp := a_timestamp
			going_right := True
		end

	go_up(a_timestamp:NATURAL_32)
			-- Fait `Current' bouger vers le haut
		require
			is_not_going_up: going_up = False
		do
			old_timestamp := a_timestamp
			going_up := True
		end

	go_down(a_timestamp:NATURAL_32)
			-- Fait `Current' bouger vers le bas
		require
			is_not_going_down: going_down = False
		do
			old_timestamp := a_timestamp
			going_down := True
		end

	jump(a_timestamp:NATURAL_32)
			-- Fait `Current' commencer � sauter
		require
			is_not_jumping: is_jumping = False
		do
			jump_timestamp := a_timestamp
			jump_y := y
			is_jumping := True
			play
		end

	stop_right
			-- Fait `Current' arr�ter de bouger vers la droite
		require
			is_going_right: going_right = True
		do
			going_right := False
			if not going_left then
				sub_image_x := animation_coordinates.first.x	-- Place l'image sans mouvement
				sub_image_y := animation_coordinates.first.y	-- Place l'image sans mouvement
			end
		end

	stop_left
			-- Fait `Current' arr�ter de bouger vers la gauche
		require
			is_going_left: going_left = True
		do
			going_left := False
			if not going_right then
				sub_image_x := animation_coordinates.first.x	-- Place l'image sans mouvement
				sub_image_y := animation_coordinates.first.y	-- Place l'image sans mouvement
			end
		end

	stop_up
			-- Fait `Current' arr�ter de bouger vers le haut
		require
			is_going_up: going_up = True
		do
			going_up := False
			if not going_up then
				sub_image_x := animation_coordinates.first.x	-- Place l'image sans mouvement
				sub_image_y := animation_coordinates.first.y	-- Place l'image sans mouvement
			end
		end

	stop_down
			-- Fait `Current' arr�ter de bouger vers le bas
		require
			is_going_down: going_down = True
		do
			going_down := False
			if not going_down then
				sub_image_x := animation_coordinates.first.x	-- Place l'image sans mouvement
				sub_image_y := animation_coordinates.first.y	-- Place l'image sans mouvement
			end
		end

	facing_left:BOOLEAN
			-- Est ce que `Current' est face � gauche

	going_left:BOOLEAN
			-- Est ce que `Current' bouge vers la gauche

	going_right:BOOLEAN
			-- Est ce que `Current' bouge vers la droite

	going_up:BOOLEAN
			-- Est ce que `Current' bouge vers le haut

	going_down:BOOLEAN
			-- Est ce que `Current' bouge vers le bas

	is_jumping:BOOLEAN
			-- Est ce que `Current' saute

	jump_timestamp: NATURAL_32

	jump_y: INTEGER


feature {NONE} -- Impl�mentation

	animation_coordinates:LIST[TUPLE[x,y:INTEGER]]
			-- Chaque coordinate de la portion de l'image dans `surface'

	old_timestamp:NATURAL_32
			-- Quand le dernier mouvement s'est fait `movement_delta'


feature {NONE} -- Constantes

	movement_delta:NATURAL_32 = 8
			-- Le delta time entre chaque mouvement de `Current'

	animation_delta:NATURAL_32 = 100
			-- Le delta time entre chaque animation de `Current'

	jump_time: NATURAL_32 = 600
			-- Le delta time du saut de `Current'

    jump_height: INTEGER = 150
			-- La hauteur du saut de `Current'


invariant

note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
