note
	description: "Background du jeu."
	author: "Pier-Olivier Chagnon"
	date: "12 F�vrier 2018"
	revision: "2.0"

class
	BACKGROUND

inherit
	IMAGE
	rename
		make as make_image
	end

create
	make

feature {NONE} -- Constructeur

	make(a_renderer:GAME_RENDERER)
			-- Initialisation de `Current' utilis� avec `a_renderer'
		require
			a_renderer.has_error = false
		local
			l_plateforme1: PLATEFORME
		do
			create {LINKED_LIST[PLATEFORME]} plateformes.make
			make_image(a_renderer, "forest1.png")
			create l_plateforme1.make (a_renderer, 0, 0, 0, 0)
			plateformes.extend (l_plateforme1)
		end

feature

	plateformes: LIST[PLATEFORME]
			-- Liste contenant les plateformes du niveau


invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
