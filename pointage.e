note
	description: "La classe {POINTAGE} permet de calculer les points"
	author: "C�dryk Coderre"
	date: "2018-04-17"
	revision: "0.1"

class
	POINTAGE

create
	make

feature {NONE} -- Constructeur

	make
			-- Permet d'initialiser un pointage.
		require
			total_a_zero: points.is_equal (0)
		do
			set_nom("")
			set_points(0)
		ensure
			total_pas_negatif: points >= 0
		end

feature -- Routines

	ajout_points (a_points:INTEGER_64)
			-- Permet d'ajouter des points
		require
			a_points >= 0
		do
			points:= points + a_points
		ensure
			total_a_changer: points /= old points
			total_pas_negatif: points >= 0
		end

	enlever_points (a_points:INTEGER_64)
			-- Permet d'enlever des points.
		require
			a_points >= 0
		do
			points:= points - a_points
		ensure
			total_a_changer: points /= old points
			total_pas_negatif: points >= 0
		end

	set_nom (a_nom: READABLE_STRING_GENERAL)
			-- Assigne `nom' avec la valeur de `a_nom'
		require
			valeur_existe: a_nom /= Void
		do
			nom := a_nom
		ensure
			nom_set: a_nom.same_string(nom)
		end


	set_points (a_points: INTEGER_64)
			-- Assigne `point' avec la valeur de `a_point'
		do
			points := a_points
		ensure
			points_set: a_points = points
		end

feature -- Attributs

	nom: READABLE_STRING_GENERAL
			-- Nom de celui qui a le pointage

	points:INTEGER_64
			-- Le total de points utilis� par `Current'

invariant
	total_positif: points >= 0
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
