note
    description : "Un jeu fait avec la librarie de Eiffel Game 2."
    author      : "C�dryk Coderre"
    author		: "Pier-Olivier Chagnon"
    generator   : "Eiffel Game2 Project Wizard"
    date        : "2018-01-31 07:14:35.866 +0000"
    revision    : "0.2"

class
	APPLICATION

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	AUDIO_LIBRARY_SHARED
	TEXT_LIBRARY_SHARED

create
	make

feature {NONE} -- Initialisation

	make
			-- Permet de commencer le jeu.
		local
			l_routeur:ROUTEUR
		do
			game_library.enable_video
			image_file_library.enable_image (true, false, false)
			audio_library.enable_playback
			text_library.enable_text
			create l_routeur.make
			if not l_routeur.has_error then
				l_routeur.run
			end
		end

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end

