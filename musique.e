note
	description: "La classe {MUSIQUE} permet de jouer un son en boucle."
	author: "C�dryk Coderre"
	date: "2018-03-26"
	revision: "0.2"

class
	MUSIQUE

inherit
	AUDIO_LIBRARY_SHARED
	GAME_LIBRARY_SHARED
	SON
		rename
			make as make_son
		redefine
			play
		end

create
	make

feature {NONE} -- Constructeur

	make
			-- Permet d'initialiser une musique
		do
			has_error := False
			make_son("restorations.ogg")
		end

feature -- Routines

	play
			-- Permet de jouer une musique en boucle.
		require else
			No_Error: not has_error
			musique_ouvert: son.is_open
		do
			son_source.queue_sound_infinite_loop (son)
			son_source.play
		ensure then
			musique_entrain_jouer: son_source.is_playing or has_error
		end

invariant
	musique_ouvert: not son.is_openable implies has_error
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
