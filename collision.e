note
	description: "Classe qui g�re la collision du personnage et des plateformes."
	author: "Pier-Olivier Chagnon"
	date: "12 Mars 2018"
	revision: "1.0"

class
	COLLISION
inherit
	GAME_LIBRARY_SHARED

feature -- Constructeur

	x:INTEGER assign set_x
			-- Position verticale de `Current'

	y:INTEGER assign set_y
			-- Position horizontale de `Current'

	set_x(a_x:INTEGER)
			-- Assigne la valeur de `x' � `a_x'
		do
			x := a_x
		ensure
			Is_Assign: x = a_x
		end

	set_y(a_y:INTEGER)
			-- Assigne la valeur de `y' � `a_y'
		do
			y := a_y
		ensure
			Is_Assign: y = a_y
		end

	sub_image_x, sub_image_y:INTEGER
			-- Position de la portion de l'image dans `surface'


	sub_image_width, sub_image_height:INTEGER
			-- Dimension de la portion de l'image dans `surface'


	valide_collision(a_objet_collision : COLLISION) : BOOLEAN
	-- Permet de valider si il y a une collision entre 2 objets
	-- Retourne vrai ou faux
		do
			Result :=
			collision_entre_objets(current, a_objet_collision)
			or
			collision_entre_objets(a_objet_collision, current)
		end

feature {NONE} -- Initialisation

	collision_entre_objets(a_objet_1, a_objet_2:COLLISION) : BOOLEAN
	-- Permet de v�rifier s'il y a une collision entre deux objets.
	-- Retourne vrai ou faux
		do
			Result :=
			(
				a_objet_1.x >= a_objet_2.x
				and
				a_objet_1.x <= a_objet_2.x + a_objet_2.sub_image_width
				and
				a_objet_1.y >= a_objet_2.y
				and
				a_objet_1.y <= a_objet_2.y + a_objet_2.sub_image_height
				) or (
				a_objet_1.x + a_objet_1.sub_image_width >= a_objet_2.x
				and
				a_objet_1.x + a_objet_1.sub_image_width <= a_objet_2.x + a_objet_2.sub_image_width
				and
				a_objet_1.y >= a_objet_2.y
				and
				a_objet_1.y <= a_objet_2.y + a_objet_2.sub_image_height
				) or (
				a_objet_1.x >= a_objet_2.x
				and
				a_objet_1.x <= a_objet_2.x + a_objet_2.sub_image_width
				and
				a_objet_1.y + a_objet_1.sub_image_height >= a_objet_2.y
				and
				a_objet_1.y + a_objet_1.sub_image_height <= a_objet_2.y + a_objet_2.sub_image_height
				) or (
				a_objet_1.x + a_objet_1.sub_image_width >= a_objet_2.x
				and
				a_objet_1.x + a_objet_1.sub_image_width <= a_objet_2.x + a_objet_2.sub_image_width
				and
				a_objet_1.y + a_objet_1.sub_image_height >= a_objet_2.y
				and
				a_objet_1.y + a_objet_1.sub_image_height <= a_objet_2.y + a_objet_2.sub_image_height
				)
		end


invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
