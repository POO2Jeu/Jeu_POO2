note
	description: "La classe {SON} permet de jouer un son sp�cifique."
	author: "C�dryk Coderre"
	date: "2018-03-26"
	revision: "0.2"

class
	SON

inherit
	AUDIO_LIBRARY_SHARED
	GAME_LIBRARY_SHARED

create
	make

feature {NONE} -- Constructeur

	make (a_chemin:STRING)
			-- Permet d'initialiser un son avec un `chemin'
		require
			chemin_non_vide: not a_chemin.is_empty
		do
			has_error := False
			create son.make (a_chemin)

			audio_library.sources_add
			son_source:=audio_library.last_source_added
			if son.is_openable then
				son.open
			else
				has_error := True
			end
		ensure
			son_bien_ouvert: son.is_open or has_error
			nombre_source: audio_library.sources_count > 0 or has_error
			derniere_source: audio_library.last_source_added = son_source
		end

feature -- Attributs

	son:AUDIO_SOUND_FILE
		-- Le fichier son � `Current'

	son_source:AUDIO_SOURCE
		-- La source du son � `Current'

	has_error:BOOLEAN
		-- Un bool�en permettant de voir si il y a eu une erreur dans `Current'

feature -- Routines

	play
		-- Permet de jouer un `son'
		require
			No_Error: not has_error
		do
			son_source.queue_sound (son)
			son_source.play
		ensure
			son_entrain_jouer: son_source.is_playing
		end

invariant
	son_ouvert: not son.is_openable implies has_error
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end

