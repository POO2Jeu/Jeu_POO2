note
	description: "Classe qui g�re les plateformes."
	author: "Pier-Olivier Chagnon"
	date: "12 Mars 2018"
	revision: "1.0"

class
	PLATEFORME

inherit
	COLLISION
	IMAGE
		rename
			make as make_image
		end

create
	make

feature {NONE} -- Constructeur

	make(a_renderer:GAME_RENDERER;a_width:INTEGER_32; a_height:INTEGER_32; a_x:INTEGER; a_y:INTEGER)
			-- Initialisation de `Current' utilis� avec `a_renderer'
		require
			a_renderer.has_error = false

		do
			make_image(a_renderer, "plateforme.png")
			sub_image_width := a_width
			sub_image_height :=  a_height
			x := a_x
			y := a_y
		ensure
			sub_image_width = a_width
			sub_image_height =  a_height
			x = a_x
			y = a_y
		end

feature -- Acc�s

	has_error: BOOLEAN
		-- Boolean pour v�rifier s'il y une erreur.


invariant
	is_x_positive: x >= 0
	is_y_positive: y >= 0

note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
