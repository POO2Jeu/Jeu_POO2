# Téléchargement

Tous les téléchargements incluent le serveur avec le jeu.

## Windows

<a href="./assets/Savage3.zip">Savage 3</a>

## Linux

<a href="./assets/Savage3.zip">Savage 3</a>

## Code Source

<a href="https://gitlab.com/POO2Jeu/Jeu_POO2/-/archive/master/Jeu_POO2-master.zip">Code Source de Savage 3</a>
