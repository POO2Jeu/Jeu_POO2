# Guide de compilation

## Windows et Linux

 1. Télécharger le projet avec la commande suivante:
    * ```git clone https://gitlab.com/POO2Jeu/Jeu_POO2.git```
 2. Ouvrir le jeu dans Eiffel Studio
 3. Pesez sur la petite flèche à côté de compiler et sélectionner Finaliser
 4. Ouvrir le projet du serveur à l’intérieur du dossier web et sélectionner la version standalone
 5. Pesez sur la petite flèche à côté de compiler et sélectionner Finaliser

# Guide d’installation

## Windows

 1. Téléchargez Eiffel Studio 18.01  de https://www.eiffel.org/ et installez.
 2. Téléchargez Eiffel_Game2.zip de https://github.com/tioui/Eiffel_Game2/tree/windows_build et installez.
 3. Copier le répertoire "game2" dans le sous-répertoire d’EiffelStudio contrib/library/
 4. Copier les fichiers dll du fichier zip correspondant à votre version d’Eiffel Studio dans le répertoire de Savage3.

## Linux

 1. Télécharger Eiffelstudio avec les commandes suivantes:
    * ```sudo add-apt-repository ppa:eiffelstudio-team/ppa```
    * ```sudo apt-get update```
    * ```sudo apt-get install eiffelstudio```
 2. Installez les librairies C avec la commande suivante:
    * ```sudo apt install git libopenal-dev libsndfile1-dev libgles2-mesa-dev libsdl2-dev libsdl2-gfx-dev libsdl2-image-dev libsdl2-ttf-dev libepoxy-dev libgl1-mesa-dev libglu1-mesa-dev```
 3. Télécharger Eiffel Game 2:
    * ```git clone --recursive https://github.com/tioui/Eiffel_Game2.git```
 4. Créer un lien entre le répertoire EiffelStudio et Eiffel_Game2
    * ```sudo ln -s `pwd`/Eiffel_Game2 /usr/lib/eiffelstudio-17.05/contrib/library/game2```
 5. Compiler la librairie:
    * ```cd Eiffel_Game2```
    * ```sudo chmod +x ./compile_c_library.sh```
    * ```./compile_c_library.sh```

# Guide de configuration

Aucune configuration requise

# Guide d’utilisation


## Windows et Linux

 1. Lancer le serveur web en premier
 2. Lancer le jeu en deuxième