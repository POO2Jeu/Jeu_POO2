# Médias

## Capture d'écran

![alt text](./assets/1.png "Image 1")

![alt text](./assets/2.png "Image 2")

![alt text](./assets/3.png "Image 3")

## Musique du jeu

### Restorations

<audio controls>
  <source src="./assets/restorations.ogg" type="audio/ogg">
  <source src="./assets/restorations.mp3" type="audio/mpeg">
</audio>
