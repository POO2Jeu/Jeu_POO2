# Informations sur le jeu

## Auteur

Cédryk Coderre et Pier-Olivier Chagnon

## Version

1.0.0

## Date

2018-05-19

## Type de jeu

Plate-forme

## But du jeu

Ce rendre à la dernière plateforme.

## Comment jouer?

C'est très simple, lorsqu'on débute une partie, il faut se rendre à la dernière plateforme le plus vite possible en allant vers la droite. On peut soumettre le pointage qu'on a fait après.
