note
	description: "Classe pour g�n�rer des images."
	author: "Pier-Olivier Chagnon"
	date: "23 Avril 2018"
	revision: "1.0"

class
	IMAGE

create
	make

feature {NONE} -- Constructeur

	make(a_renderer:GAME_RENDERER; a_image:STRING)
			-- Initialisation de `Current' avec `a_renderer'
		require
			a_renderer.has_error = false
		local
			l_image:IMG_IMAGE_FILE
		do
			has_error_image := False
			create l_image.make (a_image)
			if l_image.is_openable then
				l_image.open
				if l_image.is_open then
					create texture.make_from_image (a_renderer, l_image)
				else
					create texture.make (a_renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
					has_error_image := True
				end
			else
				create texture.make (a_renderer, create {GAME_PIXEL_FORMAT}, 1, 1)
				has_error_image := True
			end
		end

feature -- Acc�s

	texture:GAME_TEXTURE
		-- Les textures du jeu.

	has_error_image:BOOLEAN
		-- Boolean pour v�rifier s'il y a une erreur.

end
