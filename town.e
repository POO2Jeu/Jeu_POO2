note
	description: "Septi�me niveau du jeu {TOWN}."
	author: "Pier-Olivier Chagnon"
	date: "2 Mai 2018"
	revision: "1.0"

class
	TOWN

inherit
	BACKGROUND
		rename
			make as make_background
		end

create
	make

feature -- Constructeur

	make(a_renderer:GAME_RENDERER)
			-- Initialisation de `Current' utilis� avec `a_renderer'
		require
			a_renderer.has_error = false
		local
			l_plateforme1: PLATEFORME
			l_plateforme2: PLATEFORME
			l_plateforme3: PLATEFORME
			l_plateforme4: PLATEFORME
			l_plateforme5: PLATEFORME
			l_plateforme6: PLATEFORME
		do
			create {LINKED_LIST[PLATEFORME]} plateformes.make
			create l_plateforme1.make (a_renderer, 50, 20, 80, 390)
			create l_plateforme2.make (a_renderer, 30, 20, 250, 300)
			create l_plateforme3.make (a_renderer, 50, 20, 400, 220)
			create l_plateforme4.make (a_renderer, 30, 20, 800, 160)
			create l_plateforme5.make (a_renderer, 30, 20, 600, 160)
			create l_plateforme6.make (a_renderer, 30, 320, 1000, 160)
			plateformes.extend (l_plateforme1)
			plateformes.extend (l_plateforme2)
			plateformes.extend (l_plateforme3)
			plateformes.extend (l_plateforme4)
			plateformes.extend (l_plateforme5)
			plateformes.extend (l_plateforme6)
			make_image (a_renderer, "town.png")
		end


invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
