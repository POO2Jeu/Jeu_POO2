note
	description: "La classe {HTTP_HELPER} s'occupe des int�ractions entre le serveur et le client pour les pointages."
	author: "C�dryk Coderre et Pier-Olivier Chagnon"
	date: "2018-05-14"
	revision: "0.3"

class
	HTTP_HELPER

create
	make

feature {NONE} -- Constructeur

	make
		-- Permet d'initialiser le http_helper
		do
			has_error := False
		end

feature -- Routines

	deserialization(a_pointage_json:STRING_8): LIST[POINTAGE]
        -- Permet de d�s�rialiser un JSON.
    require
    	No_Error: not has_error
    local
        l_conv: JSON_SERIALIZATION
        l_factory: JSON_SERIALIZATION_FACTORY
        l_liste_pointage: LIST[POINTAGE]
    do
        l_conv := l_factory.smart_serialization
        create {ARRAYED_LIST[POINTAGE]}l_liste_pointage.make(0)
        l_conv.set_pretty_printing
        l_conv.context.deserializer_context.set_value_creation_callback (create {JSON_DESERIALIZER_CREATION_AGENT_CALLBACK}.make (
                agent (a_info: JSON_DESERIALIZER_CREATION_INFORMATION) do a_info.set_object (create {ARRAYED_LIST [POINTAGE]}.make (0)) end
            )
        )
        if attached {LIST[POINTAGE]} l_conv.from_json_string (a_pointage_json, {ARRAYED_LIST [POINTAGE]}) as la_obj then
            l_liste_pointage := la_obj
        elseif l_conv.has_deserialization_error and attached l_conv.context.deserialization_error as err then
                has_error := True
        end
        Result := l_liste_pointage
    end

	get_pointage: STRING_8
			-- Permet d'obtenir les pointages du serveur.
		require
			No_Error: not has_error
		local
			l_client: NET_HTTP_CLIENT
			l_session: HTTP_CLIENT_SESSION
			l_resultat: STRING_8
		do
			l_resultat := ""
			create l_client
			l_session := l_client.new_session("http://127.0.0.1:8080")
			has_error := not l_session.is_available
			if attached l_session.get("/pointage", Void) as la_resultat then
				if not la_resultat.error_occurred then
					if attached la_resultat.body as la_body then
						l_resultat := la_body
					end
				end
			else
				has_error := True
			end
			Result := l_resultat
		end

	post_pointage(a_pointage:POINTAGE)
			-- Permet la soumission de pointage.
		require
			No_Error: not has_error
    	local
    		l_client: NET_HTTP_CLIENT
    		l_session: HTTP_CLIENT_SESSION
    		l_context:HTTP_CLIENT_REQUEST_CONTEXT
    	do
    		create l_client
    		create l_context.make
    		l_context.add_form_parameter ("nom", a_pointage.nom)
    		l_context.add_form_parameter ("points", a_pointage.points.out)
    		l_session := l_client.new_session("http://127.0.0.1:8080")
    		has_error := not l_session.is_available
    		if attached l_session.post ("/pointage", l_context, Void) then
				has_error := False
    		else
    			has_error := True
    		end
    	end


    has_error: BOOLEAN
    	-- Boolean pour savoir s'il y a un erreur ou nom.

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
