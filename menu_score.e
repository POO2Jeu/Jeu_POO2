note
	description: "La classe {MENU_SCORE} s'occupe du menu qui montre le scoreboard."
	author: "C�dryk Coderre et Pier-Olivier Chagnon"
	date: "2018-05-14"
	revision: "0.3"

class
	MENU_SCORE

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	MENU
		redefine
			run,
			make
		end

create
	make

feature {NONE} -- Constructeur

	make(a_window:GAME_WINDOW_RENDERED)
			-- <Precursor>
	do
		PRECURSOR {MENU} (a_window)
		create http_helper.make
		create font.make ("font.ttf", 32)
		liste_pointage := http_helper.deserialization(http_helper.get_pointage)
		if font.is_openable then
			font.open
			has_error := not font.is_open
		else
			has_error := True
		end
	end

feature -- Les routines

	run
			-- <Precursor>
		do
			PRECURSOR {MENU}
		end


	on_mouse_pressed (a_timestamp: NATURAL_32; a_mouse_state: GAME_MOUSE_BUTTON_PRESSED_STATE; a_nb_clicks: NATURAL_8)
			-- <Precursor>
		do
			if a_mouse_state.is_left_button_pressed then
				if a_mouse_state.x >= 20 and a_mouse_state.x <= 120 then
					if a_mouse_state.y >= 20 and a_mouse_state.y <= 60 then
						is_menu_principal := True
						game_library.stop
					end
				end
			end
		end

	on_iteration(a_timestamp:NATURAL_32)
			-- <Precursor>
		local
			l_text_score:TEXT_SURFACE_BLENDED
			l_text_retour:TEXT_SURFACE_BLENDED
			l_text_pointage:TEXT_SURFACE_BLENDED
			l_texture_score:GAME_TEXTURE
			l_texture_retour:GAME_TEXTURE
			l_texture_pointage:GAME_TEXTURE
			l_y:INTEGER_16
			l_liste_pointage_texture:ARRAYED_LIST[GAME_TEXTURE]
		do
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (255, 255, 255))
			window.renderer.draw_filled_rectangle (0, 0, window.width, window.height)
			create l_text_score.make ("Score :", font, create {GAME_COLOR}.make_rgb (0, 0, 0))
			create l_text_retour.make ("Retour", font, create {GAME_COLOR}.make_rgb (0, 0, 0))
			create l_texture_score.make_from_surface (window.renderer, l_text_score)
			create l_texture_retour.make_from_surface (window.renderer, l_text_retour)
			window.renderer.draw_texture (l_texture_score, 20, 100)
			window.renderer.draw_texture (l_texture_retour, 20, 20)
			create l_liste_pointage_texture.make(0)
			l_y := 100

			if not http_helper.has_error then
				across
	    			liste_pointage as la_pointage
				loop
	  				create l_text_pointage.make (la_pointage.item.nom + " " + la_pointage.item.points.out, font, create {GAME_COLOR}.make_rgb (0, 0, 0))
					create l_texture_pointage.make_from_surface (window.renderer, l_text_pointage)
					l_liste_pointage_texture.extend (l_texture_pointage)
				end

				across
					l_liste_pointage_texture as la_texture
				loop
					window.renderer.draw_texture (la_texture.item, 150, l_y)
					l_y := l_y + 30
				end
			else
	  			create l_text_pointage.make ("Erreur", font, create {GAME_COLOR}.make_rgb (0, 0, 0))
				create l_texture_pointage.make_from_surface (window.renderer, l_text_pointage)
				window.renderer.draw_texture (l_texture_pointage, 150, l_y)
			end

			window.update
		end

feature -- Les attributs

	liste_pointage:LIST[POINTAGE]
			-- Liste des pointages du serveur.

	font:TEXT_FONT
			-- Utilis� pour �crire des `texts'.

	is_menu_principal:BOOLEAN
			-- Ce boolean permet de d�termin� si on retourne au menu principal ou non.

	http_helper:HTTP_HELPER
			-- Permet de faire la connection avec le serveur.

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end

