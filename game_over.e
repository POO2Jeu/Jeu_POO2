note
	description: "La classe {GAME_OVER} s'occupe de l'�cran de game over."
	author: "C�dryk Coderre et Pier-Olivier Chagnon"
	date: "2018-05-14"
	revision: "0.3"

class
	GAME_OVER

inherit
	GAME_LIBRARY_SHARED
	IMG_LIBRARY_SHARED
	MENU
		redefine
			make,
			run
		end


create
	make

feature {NONE} -- Constructeur

	make(a_window:GAME_WINDOW_RENDERED)
			-- Constructeur de `Current'
	local
		l_http_helper:HTTP_HELPER
	do
		PRECURSOR {MENU} (a_window)
		create l_http_helper.make
		create font.make ("font.ttf", 50)
		create pointage.make
		if font.is_openable then
			font.open
			has_error := not font.is_open
		else
			has_error := True
		end
	end

feature -- Routines

	run
			-- <Precursor>
		do
			window.text_input_actions.extend (agent on_text_input) -- Quand les touches du clavier sont appuy�es.
			PRECURSOR {MENU}
		end

	on_iteration(a_timestamp:NATURAL_32)
			-- <Precursor>
		local
			l_text_menu_score:TEXT_SURFACE_BLENDED
			l_text_score:TEXT_SURFACE_BLENDED
			l_text_menu_name:TEXT_SURFACE_BLENDED
			l_text_gameover:TEXT_SURFACE_BLENDED
			l_text_ok:TEXT_SURFACE_BLENDED
			l_text_pointage:TEXT_SURFACE_BLENDED
			l_text_name: TEXT_SURFACE_BLENDED
			l_texture_menu_name:GAME_TEXTURE
			l_texture_name:GAME_TEXTURE
			l_texture_menu_score:GAME_TEXTURE
			l_texture_score:GAME_TEXTURE
			l_texture_gameover:GAME_TEXTURE
			l_texture_ok:GAME_TEXTURE
			l_texture_pointage:GAME_TEXTURE
			l_y:INTEGER_16
			l_liste_pointage_texture:ARRAYED_LIST[GAME_TEXTURE]
			l_shaman: IMAGE
		do
			window.renderer.set_drawing_color (create {GAME_COLOR}.make_rgb (255, 255, 255))
			window.renderer.draw_filled_rectangle (0, 0, window.width, window.height)
			create l_text_menu_score.make ("Score :", font, create {GAME_COLOR}.make_rgb (0, 0, 0))
			if not pointage.points.out.is_empty then
				create l_text_score.make (pointage.points.out, font, create {GAME_COLOR}.make_rgb (0, 0, 0))
				create l_texture_score.make_from_surface (window.renderer, l_text_score)
				window.renderer.draw_texture (l_texture_score, 450, 250)
			end
			create l_text_menu_name.make ("Name :", font, create {GAME_COLOR}.make_rgb (0, 0, 0))
			if not pointage.nom.is_empty then
				create l_text_name.make (pointage.nom, font, create {GAME_COLOR}.make_rgb (0, 0, 0))
				create l_texture_name.make_from_surface (window.renderer, l_text_name)
				window.renderer.draw_texture (l_texture_name, 450, 325)
			end
			create l_text_gameover.make ("GAME OVER", font, create {GAME_COLOR}.make_rgb (0, 0, 0))
			create l_text_ok.make ("Ok", font, create {GAME_COLOR}.make_rgb (0, 0, 0))
			create l_texture_menu_name.make_from_surface (window.renderer, l_text_menu_name)
			create l_texture_menu_score.make_from_surface (window.renderer, l_text_menu_score)
			create l_texture_gameover.make_from_surface (window.renderer, l_text_gameover)
			create l_texture_ok.make_from_surface (window.renderer, l_text_ok)
			create l_shaman.make (window.renderer, "slime.png")
			window.renderer.draw_texture (l_texture_menu_score, 250, 250)
			window.renderer.draw_texture (l_texture_menu_name, 250, 325)
			window.renderer.draw_texture (l_texture_gameover, 350, 25)
			window.renderer.draw_texture (l_texture_ok, 475, 425)
			window.renderer.draw_texture (l_shaman.texture, 250,  130)
			create l_liste_pointage_texture.make(0)
			l_y := 100
			window.update
		end

	on_mouse_pressed (a_timestamp: NATURAL_32; a_mouse_state: GAME_MOUSE_BUTTON_PRESSED_STATE; a_nb_clicks: NATURAL_8)
			-- <Precursor>
		do
			if a_mouse_state.is_left_button_pressed then
				if a_mouse_state.x >= 475 and a_mouse_state.x <= 540 then
					if a_mouse_state.y >= 435 and a_mouse_state.y <= 475 then
						is_menu_principal := True
						game_library.stop
					end
				end
			end
		end

	on_text_input(a_timestamp:NATURAL_32; a_text:STRING_32)
			-- Quand l'utilisateur appuie une touche sur le clavier.
		do
			pointage.set_nom (pointage.nom + a_text)
		end

	set_pointage(a_pointage:POINTAGE)
			-- Permet d'assigner le pointage du joueur pour l'�cran gameover.
		require
			valeur_existe: a_pointage /= Void
		do
			pointage := a_pointage
		ensure
			pointage_set: pointage = a_pointage
		end

feature -- Attributs

	pointage:POINTAGE
		-- Nombre de points du joueur

	is_game_over:BOOLEAN
		-- Ce boolean d�termine si on acc�de au menu score ou non.

	is_starting:BOOLEAN
		-- Ce boolean d�termine si on commence le jeu ou non.

	font:TEXT_FONT
			-- Utilis� pour �crire des `texts'.

	is_menu_principal:BOOLEAN
			-- Ce boolean permet de d�termin� si on retourne au menu principal ou non.

invariant
note
	copyright: "Jeu_P002 Copyright (C) 2018"
	license:   "[
	    Jeu_POO2
    Copyright (C) 2018  POO2

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
	]"
	source: "Cegep de Drummondville"
end
